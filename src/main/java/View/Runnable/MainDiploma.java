package View.Runnable;

import Controller.Controller;

import java.io.IOException;

public class MainDiploma {
    public static void main(String[] args) throws IOException {
        Controller inter = new Controller();
        inter.consoleMenu();
    }
}
